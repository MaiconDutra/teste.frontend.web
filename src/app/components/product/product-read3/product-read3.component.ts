import { Component, OnInit } from '@angular/core';
import { ProductService } from './../product.service';
import { Product } from './../product.model';

@Component({
  selector: 'app-product-read3',
  templateUrl: './product-read3.component.html',
  styleUrls: ['./product-read3.component.css']
})
export class ProductRead3Component implements OnInit {

  products: Product[] = [];
  displayedColumns = ['id', 'name', 'price', 'action', 'noGop'];

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.productService.read().subscribe(products => {
      this.products = products;
    })
  }

}
